# Christmas Theme



## About themes

This repository is used to implement various themes at certain times to the wiki.

The following will be temporarily be replaced when these themes are used:

Wiki Navigation bar
Certain system messages (Block messages, anon message, site announcments on specific days)
Wiki background
Wiki colors

## Schedule

Down below is the list of dates used to implement the custom themes, dates that are not listed will utilize the normal theme.

***October 1st*** thru ***November 1st*** - **Halloween Theme** <br>
***December 1st*** thru ***January 1st*** - **Christmas Theme**
